from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from remindr.users.forms import UserChangeForm, UserCreationForm
from remindr.users.models import Reminder, Trigger, Criteria

User = get_user_model()

admin.site.register(Reminder)
admin.site.register(Trigger)
admin.site.register(Criteria)

@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (("User", {"fields": ("name",)}),) + auth_admin.UserAdmin.fieldsets
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["name"]
