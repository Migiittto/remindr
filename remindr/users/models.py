import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import CharField, UUIDField, ForeignKey, Model, ManyToManyField
from django.db.models.fields import TextField, IntegerField, DateTimeField
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    # First Name and Last Name do not cover name patterns
    # around the globe.
    name = CharField(_("Name of User"), blank=True, max_length=255)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})


class Device(Model):
    device_token = CharField(_("Device token"), max_length=255)
    uuid = UUIDField(default=uuid.uuid4, editable=False)
    user = ForeignKey(User, on_delete=models.CASCADE)


class Reminder(Model):
    user = ForeignKey(User, on_delete=models.CASCADE)
    title = CharField(_("Title of Reminder"), max_length=255)
    description = TextField()


class Criteria(Model):
    CRITERIA_CHOICES = [(0, "SCHEDULED")]
    CRITERIA_DIRECTION = [("gt", "GREATER THAN"), ("lt", "LESSER THAN"), ("eq", "EQUALS")]

    type = IntegerField(default=0, choices=CRITERIA_CHOICES)
    value_time = DateTimeField()
    value_direction = CharField(max_length=2, choices=CRITERIA_CHOICES, default="gt")
    value_accuracy = IntegerField(default=0)


class Trigger(Model):
    reminder = ForeignKey(Reminder, on_delete=models.CASCADE)
    choices = ManyToManyField(Criteria)
