from django.conf.urls import url
from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from remindr.users.views import (
    user_detail_view,
    user_redirect_view,
    user_update_view, random)



app_name = "users"
urlpatterns = [
    path("~redirect/", view=user_redirect_view, name="redirect"),
    path("detail/<str:username>/", view=user_detail_view, name="detail"),
    path("update/", view=user_update_view, name="update"),
    path("random/", view=random, name="random"),
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]
