from rest_framework import serializers

from remindr.users.models import User, Device


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "first_name", "last_name", "url", "id"]

        extra_kwargs = {
            "url": {"view_name": "api:user-detail", "lookup_field": "username"}
        }


class DeviceSerializer(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()

    def get_token(self, obj):
        return obj.device_token
    class Meta:
        model = Device
        fields = ['id', 'uuid', 'token']
